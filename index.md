---
theme: default
title: Data Driven Process
description: Please change this with a one-line description of your presentation
author: Shady, Ramez & Rodrigo
keywords: marp,marp-cli,slide
url: Please replace this with the URL of the deployed presentation
marp: true
image: Please replace this with a URL of an avatar/icon of your presentation
paginate: true
---

# Data Driven Process
## An efficient way to store materials

#### Ramez, Shady and Rodrigo
---

<style>
img[alt~="center"], .center, .fence {
  display: block;
  margin: 0 auto;
}
kbd {
    background-color: #eee;
    border-radius: 3px;
    border: 1px solid #b4b4b4;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .2), 0 2px 0 0 rgba(255, 255, 255, .7) inset;
    color: #333;
    display: inline-block;
    font-size: .85em;
    font-weight: 700;
    line-height: 1;
    padding: 2px 4px;
    white-space: nowrap;
}
.float-right {
    float: right;
}
.fence img {
    width: auto;
    max-width: 100%;
    height: auto;
    max-height: 100%;
}
.margin-top {
    margin-top: 1em;
}
.padding-top {
    padding-top: 1em;
}
</style>

# Collected Stories

> "For a whole 4 months I walked above metal rebars that was laying infront of my office door in the construction site", said a Site Architect :construction_worker:

> "The storage area was completely filled up with cement bags, so we decided to distribute the excess cement bags into a site station where it should be used after 2-3 days, however during those days cement bags was damaged, corrupted and even there were missing bags." said a Civil Engineer :construction_worker:

> "The workforce is using the paint buckets to work on them , as the buckets is scattered all over the place.", said a Site Architect :construction_worker:

---
# Construction Site storage issues

Failure to prepare the site layout ahead of time is a major source of operational inefficiency and can significantly increase the overall cost of a project. The following issues may arise in the absence of a detailed site layout plan:

* Material stacks wrongly located. Materials arriving on site are off-loaded into what someone guesses to be the correct location. This problem may involve double or triple handling of materials to another location.
* Plant and equipment wrongly located.
* Inadequate space allowed. Where inadequate space is allowed for the stacking of materials or activities
* Site huts wrongly located in relation to their effective use
---
# Mindmap
<br/><br/>
<br/><br/>
<br/><br/>
<br/><br/>
![bg 75%](./assets/Mindmap_UML.png)

---
# How does it works?

This plugin collects the information from the supply of materials schedule and quantities.   
With this schedule a map of the storage zone in the site will be generated to look where the materials could be sorted with the best efficiency.

---
# How it works - Sketch

::: fence

@startuml
start
 if (Update Parameters to Create Output A) then 
  : Export Output A in a Form of Report ;
else 
 -Start Site Layout Simulation
- Divide Output A to Tasks 
- Set Location for each task in the Site Boundary Parameter
if ( Task Required at current Location ) then (yes)
 : Keep in set location ;
else (no)
 -Move to another location
 - Task needed in diffrent location later 
 -Create task timeline
endif
- Create DWG Layouts of all tasks loactions thru timeline 
- Export all DWG 
endif
- End Simulation

@enduml

:::

---
# Timeline
## How are we going to work?  
<br/><br/>
<br/><br/>
<br/><br/>
![bg 95%](assets/gantt_chart.JPG)

---
# Open Sources to be integrated

* [GreaterWMS](https://github.com/Singosgu/GreaterWMS) Open source warehouse management system. API uses restful protocol to facilitate for add-on functions development.
<!--* [modern-data-warehouse-dataops](https://github.com/Azure-Samples/)-->
* [WDA-Main](https://github.com/shucheng-ai/WDA-main) An AI tool to generate warehouse layout design in 2D and 3D format. WDA can interpret warehouse structures from CAD drawings, and generate layout designs of inventory.
* [RackLay](https://github.com/Avinash2468/RackLay) Monocular Multi-Layered Layout Estimation for Warehouses with Sim2Real Transfer
* [js-Simulator](https://github.com/chen0040/js-simulator) Is a general-purpose discrete-event multiagent simulator for agent-based modelling and simulation. Agent-Based Modelling and Simulation. It was written entirely in Javascript.
---
# Thank you for your attention!

<!--
# Nowadays...

Whereas in construction works nowadays , field practitioners manually mark up a single site drawing to include major temporary facilities needed on site throughout the duration of the project. They depend on knowledge of years of experience, common sense, and adoption of past layouts in determining positions of temporary facilities on site. But, they can not keep track of all factors that could affect the selection, location, and interactions of all facilities to be positioned.

>

<!--
# The Problem of Building Material Supply Chain

Most of these problems are not generated in the conversion process but in the different interfaces that exist within the supply chain. Some of the general problems are as follows:

* Design problems (many changes and inconsistent information).
* Poor quality of materials and components.
* Deficient communication and information transfer.
* Inadequate management within the supply chain, mainly poor planning and control.
* Lack of effective methods for measuring the performance of the different parties.

---
>